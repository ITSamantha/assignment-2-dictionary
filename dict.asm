%include "lib.inc"
section .text

global find_word
; rdi - указатель на строку
; rsi - указатель на ноду списка
find_word:
.loop:
	cmp rsi, 0
	je .end
	push rdi
	push rsi
	add rsi, 8 ;shift for id
	call string_equals
	pop rsi
	pop rdi
	cmp rax, 0
	jne .found
	mov rsi, [rsi]
	jmp .loop
	
.found:
	mov rax, rsi
	ret
.end:
	xor rax, rax
	ret
	
	
	
	