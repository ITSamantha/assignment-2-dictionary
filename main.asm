%include "words.inc"
%include "lib.inc"
%define stderr 2
%define ERR_CODE_NOT_FOUND 300
%define ERR_CODE_OVERFLOW 400
%define MAX_INPUT_STR_LEN 256
section .rodata
enter_string_msg: db "Enter a key to find it in the dictionary: ", 0
found_msg: db "Found key, its value: ", 0
not_found_msg: db "There isn't such key!", 0
too_long_string_msg1: db "Entered word should be less than ", 0
too_long_string_msg2: db " symbols!", 0
section .bss
buffer: resb MAX_INPUT_STR_LEN
section .text

global _start
extern find_word

_start:
	mov rdi, enter_string_msg ;entering msg
	call print_string
	
	mov rdi, buffer ;reading word
	mov rsi, MAX_INPUT_STR_LEN
	call read_word
	
	cmp rax, 0 ;check for overflow
	je .err_too_long_string
	
	mov rdi, buffer ;finding in dictionary
	mov rsi, next_node
	call find_word
	
	cmp rax, 0 ;check if not found
	je .err_not_found
	
	push rax
	
	mov rdi, found_msg ;msg about find
	call print_string
	
	pop rax
	
	add rax, 8 ;shift for id
	mov rdi, rax
	
	push rdi
	call string_length ;counting key's len
	pop rdi
	
	add rdi, rax ;shift for key
	inc rdi
	
	call print_string ;printing value
	call print_newline
	
	xor rdi, rdi
	jmp exit
	
.err_not_found: ;err not found
	mov rdi, not_found_msg
	call print_error
	call print_newline
	mov rdi, ERR_CODE_NOT_FOUND
	jmp exit
.err_too_long_string: ; err overflow
	mov rdi, too_long_string_msg1
	call print_error
	mov rdi, MAX_INPUT_STR_LEN
	call print_uint
	mov rdi, too_long_string_msg2
	call print_error
	call print_newline
	mov rdi, ERR_CODE_OVERFLOW
	jmp exit	
	
	
