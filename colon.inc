%define next_node 0

%macro colon 2

%ifid %2

%ifstr %1

%2:

	dq next_node

	db %1, 0

	%define next_node %2
	
%else

	%fatal "The first arg isn't string"

%endif

%else

	%fatal "The second arg isn't id"

%endif

%endmacro